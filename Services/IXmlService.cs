using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApi.Models;

namespace WebApplication.Services
{
    public interface IXmlService
    {
        IEnumerable<ToDoItem> List();
    }
}
