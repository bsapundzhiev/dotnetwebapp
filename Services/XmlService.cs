using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApi.Interfaces;
using ToDoApi.Models;

namespace WebApplication.Services
{
    public class XmlService : IXmlService
    {
        private readonly IListItemRepository _toDoRepository;

        public XmlService(IListItemRepository repo) {
            _toDoRepository = repo;
        }

        public IEnumerable<ToDoItem> List() {

            return _toDoRepository.All; 
        }
    }
}